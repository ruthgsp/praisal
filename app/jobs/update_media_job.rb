class UpdateMediaJob
  include SuckerPunch::Job

  def perform(user_id)
    ActiveRecord::Base.connection_pool.with_connection do
      user = User.find(user_id)

      media = user.new_media

      media.each do |picture|
        Picture.picture_from_instagram(picture)
        puts "NEW PICTURE: #{picture.id} from #{picture.user.username}"
      end
    end
  end
end
