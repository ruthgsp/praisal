class PictureUpdaterJob
  include SuckerPunch::Job

  def perform
    puts 'Picture Updater going in.'
    ActiveRecord::Base.connection_pool.with_connection do
      Picture.where('updated_at < ? AND posted_at > ?', 1.minute.ago, 2.weeks.ago).find_each(batch_size: 10) do |picture|
        puts "Picture Updater: Updating #{picture.instagram_id}"
        picture.update_self
        puts "Picture Updater: Done updating #{picture.instagram_id}"
        sleep(1)
      end
    end
    puts 'Picture Updater going out.'
    PictureUpdaterJob.new.async.later(1)
  end

  def later(sec)
    after(sec) { perform }
  end
end
