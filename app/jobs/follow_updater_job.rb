class FollowUpdaterJob
  include SuckerPunch::Job

  def perform
    puts 'Follow Updater Job going in.'
    ActiveRecord::Base.connection_pool.with_connection do
      User.where('dummy = ?', false).find_each(batch_size: 10) do |user|
        followers = user.all_followers.map do |f|
          f.id
        end

        follower_cache = Follow.where(following_id: user.instagram_id)
        following_cache = Follow.where(follower_id: user.instagram_id)

        cache = follower_cache.map do |f|
          f.follower_id.to_s
        end

        (followers-cache).each do |f|
          puts "FOUND A FOLLOWER: #{f}"
          alt = following_cache.where(following_id: f).first
          Follow.new(follower_id: f, following_id: user.instagram_id, reverse: alt.present?).save!
          log = FollowLog.where(user_id: user.instagram_id, follower_id: f).first
          if log.present?
            log.added = true
            log.save!
          else
            FollowLog.new(user_id: user.instagram_id, added: true, follower_id: f).save!
          end
          if alt.present?
            alt.reverse = true
            alt.save!
          end
        end

        (cache-followers).each do |f|
          puts "LOST A FOLLOWER: #{f}"
          follow = follower_cache.where(follower_id: f).first
          opp_follow = following_cache.where(following_id: f).first
          Follow.delete(follow.id)
          log = FollowLog.where(user_id: user.instagram_id, follower_id: f).first
          if log.present?
            log.added = false
            log.save!
          else
            FollowLog.new(user_id: user.instagram_id, added: false, follower_id: f).save!
          end
          if opp_follow.present?
            opp_follow.reverse = false
            opp_follow.save!
          end
        end

        following = user.all_following.map do |f|
          f.id
        end
        cache = following_cache.map do |f|
          f.following_id.to_s
        end
        (following-cache).each do |f|
          puts "NOW FOLLOWING: #{f}"
          alt = follower_cache.where(follower_id: f).first
          Follow.new(follower_id: user.instagram_id, following_id: f, reverse: alt.present?).save!
          if alt.present?
            alt.reverse = true
            alt.save!
          end
        end

        (cache-following).each do |f|
          puts "STOPPED FOLLOWING: #{f}"
          follow = following_cache.where(following_id: f).first
          alt = follower_cache.where(follower_id: f).first
          Follow.delete(follow.id)
          if alt.present?
            alt.reverse = false
            alt.save!
          end
        end
      end
      return
      sleep(1)
    end

    puts 'Follow Updater Job going out.'
    FollowUpdaterJob.new.async.later(3600)
  end

  def later(sec)
    puts 'Follow Updater: Starting Delay'
    after(sec) { perform }
  end
end
