class UserUpdaterJob
  include SuckerPunch::Job

  def perform
    puts 'User Updater going in.'
    ActiveRecord::Base.connection_pool.with_connection do
      User.where('updated_at < ?', 5.minutes.ago).find_each(batch_size: 10) do |user|
        puts "User Updater: Updating #{user.instagram_id}"
        user.update_media(false)
        user.update_self(user.client.user_info(user.instagram_id))
        puts "User Updater: Done updating #{user.instagram_id}"
      end
    end
    puts 'User Updater going out.'
    UserUpdaterJob.new.async.later(120)
  end

  def later(sec)
    puts 'User Updater: Starting Delay'
    after(sec) { perform }
  end
end
