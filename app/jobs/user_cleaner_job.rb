class UserCleanerJob
  include SuckerPunch::Job

  def perform
    puts 'User Cleaner going in.'
    ActiveRecord::Base.connection_pool.with_connection do
      User.where('created_at < ? AND dummy = ?', 1.day.ago, true).find_each(batch_size: 10) do |user|
        puts "User Updater: Deleting #{user.instagram_id}"
        User.delete(user.id)
      end
    end
    puts 'User Updater going out.'
    UserCleanerJob.new.async.later(1200)
  end

  def later(sec)
    puts 'User Updater: Starting Delay'
    after(sec) { perform }
  end
end
