class BetUpdaterJob
  include SuckerPunch::Job

  def perform
    puts 'Bet Updater going in.'
    pusher = Grocer.pusher(
      certificate: "#{Rails.root}/dev_certificate.pem",
      gateway: 'gateway.sandbox.push.apple.com'
    )
    ActiveRecord::Base.connection_pool.with_connection do
      Bet.where('end_time < ? AND collected = ?', Time.current, false).find_each(batch_size: 10) do |bet|
        puts "Bet Updater: Updating #{bet.id}"

        if bet.picture.nil? || bet.picture.large_link.nil?
          bet.delete_self
        else
          bet.final_likes = bet.picture.likes
          bet.collected = true
          bet.save!
          if bet.won?
            puts 'User won bet!'
            better = bet.better
            better.money += bet.betted_money * 2
            better.save!
          else
            puts 'User lost bet'
          end

          if bet.better.device_token.present?
            better = bet.better
            better.notification_count = better.notification_count.to_i + 1
            better.save!
            notif = Grocer::Notification.new(
              device_token:      better.device_token,
              alert:             "One of your bets just finished!",
              badge:             better.notification_count)
            pusher.push(notif)
          end
        end
        puts "Bet Updater: Done updating #{bet.id}"
      end
      next_bet = Bet.where('end_time >= ?', Time.current).order('end_time ASC').first
      delay = next_bet.present? ? next_bet.end_time - Time.current : 1.day
      puts "Bet Updater going out, sleeping #{delay}."
      BetUpdaterJob.new.async.later(delay)
    end
  end

  def later(sec)
    after(sec) { perform }
  end
end
