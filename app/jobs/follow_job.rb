class FollowJob
  include SuckerPunch::Job

  def perform(user_id)
    puts 'Follow Job going in.'
    ActiveRecord::Base.connection_pool.with_connection do
      user = User.find(user_id)
      followers = user.all_followers

      followers.each do |follower|
        if Follow.where(follower_id: follower.id, following_id: user.instagram_id).first.nil?
          alt = Follow.where(following_id: follower.id, follower_id: user.instagram_id).first
          Follow.new(follower_id: follower.id, following_id: user.instagram_id, reverse: alt.present?).save!
          if alt.present?
            alt.reverse = true
            alt.save!
          end
          puts "FOUND A FOLLOWER: #{follower.id}"
        end
      end

      following = user.all_following
      following.each do |follow|
        if Follow.where(follower_id: user.instagram_id, following_id: follow.id).first.nil?
          alt = Follow.where(follower_id: follow.id, following_id: user.instagram_id).first
          Follow.new(follower_id: user.instagram_id, following_id: follow.id, reverse: alt.present?).save!
          if alt.present?
            alt.reverse = true
            alt.save!
          end
          puts "FOUND SOMEONE TO FOLLOW: #{follow.id}"
        end
      end
    end

    puts 'Follow Job going out.'
  end
end
