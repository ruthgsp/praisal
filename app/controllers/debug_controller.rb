class DebugController < ApplicationController
  skip_before_filter :check_session
  def set_points
    current_user.money = params[:points]
    current_user.save!
    render json: { success: true, money: current_user.money }
  end

  def start_updater
    render json: { success: true }
  end

  def stop_updater
    render json: { success: true }
  end

  def get_auth_token
    user = User.find_by_username(params[:username])
    render json: { success: true, auth_token: user.auth_token }
  end

  def push
    pusher = Grocer.pusher(certificate: "#{Rails.root}/dev_certificate.pem", gateway: 'gateway.sandbox.push.apple.com')
    feedback = Grocer.feedback(certificate: "#{Rails.root}/dev_certificate.pem")
    notif = Grocer::Notification.new(
      device_token:      current_user.device_token,
      alert:             'Test',
      badge:             1,
      content_available: true)

    puts pusher.push(notif)
    feedback.each do |attempt|
      puts "Device #{attempt.device_token} failed at #{attempt.timestamp}"
    end
    render json: { success: true }
  end

  def device_token
    user = User.find_by_username(params[:username])
    render json: { device_token: user.device_token }
  end

  def access_token
    user = User.find_by_username(params[:username])
    render json: { device_token: user.access_token }
  end

  def clear_bets
    Bet.all.each do |bet|
      if bet.picture.nil?
        Bet.delete(bet.id)
      end
    end
    render json: {success: true}
  end

  def check_follows
    Follow.find_each do |f|
      if Follow.where(following_id: f.following_id, follower_id: f.follower_id).all.count > 1
        Follow.delete(f.id)
      end
    end
    render json: {success: true}
  end

  def update_pictures
    user = User.find_by_username(params[:username])
    user.pictures.each do |pic|
      Picture.delete(pic.id)
    end
    user.update_media(false)
    render json: {success:true, pics: user.pictures}
  end
end
