# Handles all things bets.

class BetsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  def index
    start = if params[:page].nil?
              0
            else
              (params[:page].to_i - 1) * 30
    end
    bets = current_user.bets.slice(start, start + 30)
    bets ||= []
    render json: { success: true, bets: bets }
  end

  # photo_id
  # likes
  # amount
  def create
    fail UnprocessableEntityError, 'Already betted' if Bet.find_by_better_id_and_picture_id(current_user.instagram_id, params[:photo_id]).present?
    fail UnprocessableEntityError, 'Not enough money' if current_user.money < params[:amount].to_i
    fail UnprocessableEntityError, 'Invalid amount' if params[:amount].to_i <= 0

    if picture.nil?
      picture_info = current_user.client.picture_info(photo_id)
      Picture.picture_from_instagram(picture_info)
    end

    current_user.money -= params[:amount].to_i
    current_user.save!

    Bet.new(
      better: current_user,
      picture: picture,
      betted_likes: params[:likes],
      betted_money: params[:amount],
      end_time: 1.day.from_now,
      poster_id: picture.poster_id
    ).save!

    render json: { success: true, money: current_user.money }
  end

  private

  def photo_id
    params[:photo_id]
  end

  def picture
    Picture.find_by_instagram_id(photo_id)
  end
end
