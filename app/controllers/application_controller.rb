# Highest level controller.

class ApplicationController < ActionController::Base
  skip_before_filter :verify_authenticity_token
  before_filter :check_session

  class BadRequestError < StandardError; end
  class UnprocessableEntityError < StandardError; end
  class UnauthorizedError < StandardError; end

  # rescue_from StandardError do |e|
  #   render json: { success: false, message: e.message }
  # end

  protect_from_forgery with: :null_session

  private

  def current_user
    @current_user ||= auth_token && User.find_by_auth_token(auth_token)
  end

  def auth_token
    params[:auth_token]
  end

  def check_session
    fail UnauthorizedError, 'Invalid auth token' if current_user.nil?
  end
end
