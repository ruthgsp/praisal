class UsersController < ApplicationController
  def index
    render json: current_user.as_json(show_auth_token: true)
  end

  def show
    if params[:id] != current_user.instagram_id && Follow.where(follower_id: current_user.instagram_id, following_id: params[:id]).first.nil?
      render json: { success: false, message: 'User not following requested user.', is_following: false }
    else
      user = User.find_by_instagram_id(params[:id])
      if user.nil? || user.dummy?
        user_info = current_user.client.user_info(params[:id])
        if user_info.nil?
          render json: { success: false, message: 'User does not have permission to view this user.' }
        end
        user = User.user_from_instagram(user_info, current_user.access_token, true, true)
        json = { user: user.as_json(show_stats: true), success: true, is_following: true }
        json[:user][:pictures] = user.top_media.map { |media| Picture.picture_from_instagram(media, true) }.sort_by { |pic| -pic.likes }.slice(0, 9)
        json[:user][:wins] = 0
        render json: json
      else
        render json: { user: user.as_json(show_pictures: true, show_stats: true), success: true, is_following: true }
      end
    end
  end

  def feed
    if current_user.is_new?
      current_user.is_new = false
      current_user.save
    end
    render json: { feed: current_user.feed(params[:newest_id]), success: true }
  end

  def trending_photos
    trends = []
    feed = current_user.client.all_feed.select { |item| (item.likes.to_f / item.poster.followers) > (item.average / 100.0) }.sort_by do |pic|
      if pic.poster.followers == 0
        0
      else
        -pic.likes.to_f / pic.poster.followers
      end
    end

    feed.select do |photo|
      if BlockedUser.show_photo?(photo.poster.id.to_s, photo.id, current_user.id)
        trends.append(photo)
      end
    end 

    render json: { feed: trends, success: true }
  end

  def friends
    query = params[:q].to_s.upcase

    friends = []
    followings = current_user.followings
    counter = start
    i = 0
    while i < followings.length && friends.length < 50
      friend = followings[i].following
      if friend.present?
        should_include = false
        if friend.full_name.upcase.include?(query)
          should_include = true
        end

        if friend.username.present? && friend.username.upcase.include?(query)
          should_include = true
        end

        if should_include && counter == 0
          friends.append(friend)
        elsif should_include
          counter -= 1
        end
      end

      i += 1
    end

    friends.sort_by! { |friend| [friend.full_name, friend.username] }

    render json: { success: true, friends: friends }
  end

  def make_friend
    current_user.client.follow(params[:id])
    alt = Follow.where(follower_id: params[:id], following_id: current_user.instagram_id).first
    Follow.new(follower_id: current_user.instagram_id, following_id: params[:id], reverse: alt.present?).save!
    if alt.present?
      alt.reverse = true
      alt.save!
    end
    user = User.find_by_instagram_id(params[:id])
    if user.nil? || user.dummy?
      user_info = current_user.client.user_info(params[:id])
      if user_info.nil?
        render json: { success: false, message: 'User does not have permission to view this user.' }
      end
      user = User.user_from_instagram(user_info, current_user.access_token, true, true)
      json = { user: user.as_json(show_stats: true), success: true }
      json[:user][:pictures] = user.top_media.map { |media| Picture.picture_from_instagram(media, true) }.sort_by { |pic| -pic.likes }.slice(0, 9)
      json[:user][:wins] = 0
      render json: json
    else
      render json: { user: user.as_json(show_pictures: true, show_stats: true), success: true, is_following: true }
    end
  end

  def invite_friend
    render json: { success: true, money: current_user.money }
  end

  def rich_friends
    render json: { users: current_user.followings.map(&:following).compact.push(current_user).sort_by { |friend| -friend.money }.slice(start, 50), success: true }
  end

  def popular_friends
    render json: { users: current_user.followings.map(&:following).compact.push(current_user).sort_by { |friend| -friend.average[0] }.slice(start, 50).map { |friend| friend.as_json(show_average: true) }, success: true }
  end

  def all_friends
    query = params[:q].to_s.upcase

    friends = []
    followings = current_user.followings
    i = start
    while i < followings.length && friends.length < 50
      friend = followings[i].following(true)
      if friend.present?
        should_include = false
        if friend.full_name.present? && friend.full_name.upcase.include?(query)
          should_include = true
        end

        if friend.full_name.empty? && friend.username.present? && friend.username.upcase.include?(query)
          should_include = true
        end

        if should_include
          friends.append(friend)
        end
      end

      i += 1
    end

    friends.sort_by! { |friend| [friend.full_name, friend.username] }

    render json: { success: true, friends: friends }
  end

  def new_followers
    followers = FollowLog.where('updated_at > ? AND user_id = ?', [1.week.ago, current_user.created_at + 1.day].max, current_user.instagram_id).map do |log|
      follower = log.follower
      follower.as_json.merge(gained: log.added) if follower
    end.compact
    #followers = Follow.where('created_at > ? AND following_id = ?', [1.week.ago, current_user.created_at + 1.day].max, current_user.instagram_id).map do |follow|
    #  follower = follow.follower(true)
    #  follower.as_json.merge(gained: true) if follower
    #end.compact

    render json: { success: true, users: followers, header: { gained: followers.select{|f| f[:gained] }.count, lost: followers.select{|f| f[:gained]}.count } }
  end

  def redeem_invite
    if params[:id] == 'bad_user'
      render json: { succes: false, message: 'Invalid username' }
    else
      render json: { success: true, money: current_user.money }
    end
  end

  def best_followers
    followers = current_user.best_followers
    count = followers.length
    if params[:previous_id].present?
      followers = followers.drop_while { |f| f.id != params[:previous_id] }
      followers = followers.drop(1)
    end

    to_return = followers.slice(0, 50)

    if to_return.length < 50 && params[:previous_id].nil?
      count = to_return.length
    end

    render json: { success: true, users: to_return, count: count }
  end

  def not_followers
    follows = Follow.where('follower_id = ? AND reverse = ?', current_user.instagram_id, false)
    count = follows.length
    if params[:previous_id].present?
      previous_follow = Follow.where('follower_id = ? AND following_id = ?', current_user.instagram_id, params[:previous_id]).first
      if previous_follow.nil?
        fail UnprocessableEntityError, 'Invalid previous id'
      end

      follows = follows.drop(follows.index(previous_follow) + 1)
    end

    to_return = follows.slice(0, 50).map { |follow| follow.following(true) }.compact

    if to_return.length < 50 && params[:previous_id].nil?
      count = to_return.length
    end

    render json: { success: true, users: to_return, count: count }
  end

  def flag_photo
    #params poster.id photo.id reason
    block_report = BlockedUser.generate_block_user(params[:poster_id])
    user_report = UserReport.new(
      blocked_user_id: block_report.id,
      blocked_by: current_user.id,
      block_this: block_report.user_id,
      blocked_instagram_id: block_report.instagram_id,
      photo_id: params[:photo_id],
      reason: params[:reason]
    )
    user_report.save!
    render json: { success: true }
  end

  def not_following
    followings = Follow.where('following_id = ? AND reverse = ?', current_user.instagram_id, false)
    count = followings.length
    if params[:previous_id].present?
      previous_follow = Follow.where('following_id = ? AND follower_id = ?', current_user.instagram_id, params[:previous_id]).first
      if previous_follow.nil?
        fail UnprocessableEntityError, 'Invalid previous id'
      end

      followings = followings.drop(followings.index(previous_follow) + 1)
    end

    to_return = followings.slice(0, 50).map { |follow| follow.follower(true) }.compact

    if to_return.length < 50 && params[:previous_id].nil?
      count = to_return.length
    end

    render json: { success: true, users: to_return, count: count }
  end

  def store_device_token
    user = current_user
    user.device_token = params[:device_token]
    user.save!
    render json: { success: true }
  end

  def clear_notifications
    user = current_user
    user.notification_count = 0
    user.save!
    render json: { success: true }
  end

  def start
    if params[:page].nil?
      0
    else
      (params[:page].to_i - 1) * 50
    end
  end
end
