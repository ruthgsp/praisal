require 'securerandom'

class OauthController < ApplicationController
  include InstagramApi
  skip_before_filter :check_session

  def authorize
    render json: {
      success: true,
      url: authorization_url }
  end

  def login
    fail BadRequestError, 'Missing code' if params[:code].nil?
    fail UnprocessableEntityError, 'User already logged in' if current_user
    response = exchange_code(params[:code])

    fail BadRequestError, 'Invalid code' if response.nil?

    user_info = InstagramClient.new(response.access_token).user_info

    user = User.find_by_instagram_id(user_info.id)

    is_valid = true
    is_new = false

    if user.nil?
      is_new = true
      user = User.user_from_instagram(user_info, response.access_token)
      is_valid = false if user.nil?
    elsif user.dummy?
      is_new = true
      user.dummy = false
      user.access_token = response.access_token
      user.save!
    end

    if is_valid
      if user.login_count == 0
        user.auth_token = SecureRandom.urlsafe_base64
        user.save!
      end

      user.access_token = response.access_token
      user.save!

      user.login_count += 1
      user.save!

      user.update_media
      user.update_relationships

      render json: {
        success:  true,
        user:     user.as_json(show_auth_token: true),
        is_new:   user.is_new }
    else
      render json: {
          success: false,
          message: "Sorry, only users with less than 100,000 followers are allowed to use this app."
      }
    end
  end

  def logout
    fail UnprocessableEntityError, 'No user logged in' if current_user.nil?

    current_user.login_count -= 1

    if current_user.login_count == 0
      current_user.auth_token = nil
    end

    current_user.save!

    render json: {
      success: true }
  end

  def debug_login
    user = User.find_by_username(params[:username]).first

    if user.login_count == 0
      user.auth_token = SecureRandom.urlsafe_base64
      user.save!
    end

    user.login_count += 1
    user.save!

    render json: { user: user, auth_token: user.auth_token }
  end
end
