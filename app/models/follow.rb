class Follow < ActiveRecord::Base
  # Follower -> Following
  def follower(replace = false)
    user = User.find_by_instagram_id(follower_id.to_s)
    if !replace && user.present? && user.dummy?
      user = nil
    end
    if replace && user.nil?
      user_info = following.client.user_info(follower_id)
      if user_info.present?
        user = User.temp_user(user_info, following.access_token)
        user.save!
      else
        user = nil
      end
    end
    user
  end

  def following(replace = false)
    user = User.find_by_instagram_id(following_id.to_s)
    if !replace && user.present? && user.dummy?
      user = nil
    end
    if replace && user.nil?
      user_info = follower.client.user_info(following_id)
      if user_info.present?
        user = User.temp_user(user_info, follower.access_token)
        user.save!
      else
        user = nil
      end
    end
    user
  end
end
