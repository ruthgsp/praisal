class Bet < ActiveRecord::Base
  belongs_to :better, class_name: 'User', inverse_of: :bets, primary_key: 'instagram_id'
  belongs_to :picture, inverse_of: :bets, primary_key: 'instagram_id'
  def as_json(*)
    average = better.client.average(poster_id)
    if picture.nil?
      {
        picture: picture,
        betted_likes: betted_likes,
        betted_money: betted_money,
        final_likes: final_likes.to_f,
        end_time: end_time,
        payout: won? ? 2 * betted_money : 0,
        done: done?,
        average: average[0],
        won: won?
      }
    else
      {
        picture: picture.as_json(show_full_poster: true),
        betted_likes: betted_likes,
        betted_money: betted_money,
        final_likes: final_likes.to_f,
        end_time: end_time,
        average: average[0],
        payout: won? ? 2 * betted_money : 0,
        done: done?,
        won: won?
      }
    end
  end

  def won?
    return false unless done?

    average = better.client.average(poster_id)
    if betted_likes == 0
      final_likes.to_f / average[1] <= average[0] / 100
    else
      final_likes.to_f / average[1] >= average[0] / 100
    end
  end

  def done?
    end_time < DateTime.current
  end

  def delete_self
    pusher = Grocer.pusher(
      certificate: "#{Rails.root}/dev_certificate.pem",
      gateway: 'gateway.sandbox.push.apple.com'
    )
    better.money += 100

    if better.device_token.present?
      better.notification_count = better.notification_count.to_i + 1
      notif = Grocer::Notification.new(
        device_token:      better.device_token,
        alert:             "One of your bet's pictures was deleted.",
        badge:             better.notification_count)
      pusher.push(notif)
    end
    better.save!
    Bet.delete(id)
  end
end
