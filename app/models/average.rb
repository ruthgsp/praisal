class Average < ActiveRecord::Base
  def value
    return [0, followers] if pictures == 0
    value = total.to_f / pictures / followers * 100
    [value, followers]
  end
end
