require 'instagram'

class InstagramClient
  def initialize(access_token)
    @access_token = access_token
  end

  def user_info(user_id = nil)
    return client.user(user_id)
  rescue
    return nil
  end

  def following
    response = client.user_follows
    following = []

    following.concat(response)

    while response.pagination.next_cursor.present?
      response = client.user_follows(cursor: response.pagination.next_cursor)

      following.concat(response)
    end

    following
  end

  def followers
    response = client.user_followed_by
    followers = []

    followers.concat(response)

    while response.pagination.next_cursor.present?
      response = client.user_followed_by(cursor: response.pagination.next_cursor)

      followers.concat(response)
    end

    followers
  end

  def feed(max_id = nil)
    feed = client.user_media_feed(count: 10, max_id: max_id)
    feed.map! do |media|
      filtered = filter_and_strip_media(media)
      if filtered.present?
        average = average(filtered.poster.id)
        filtered.average = ('%.2f' % average.first).to_f
        filtered.poster.followers = average.last

        picture = Picture.find_by_instagram_id(filtered.id)

        if picture.nil?
          filtered.bets = { over: 0, under: 0 }
        else
          over = picture.bets.select { |bet| bet.betted_likes == 100 }.count
          filtered.bets = { over: over, under: picture.bets.count - over }
        end
        filtered
      end
    end.compact
  end

  def all_feed
    to_return = []
    max_id = nil
    loop do
      feed = client.user_media_feed(count: 100, max_id: max_id)

      new_feed = feed.map do |media|
        filtered = filter_and_strip_media(media, 2 * 24 * 60 * 60)
        if filtered.present?
          average = average(filtered.poster.id)
          filtered.average = ('%.2f' % average.first).to_f
          filtered.poster.followers = average.last
        end
        filtered
      end.compact

      to_return += new_feed

      puts feed.last
      if feed.last.nil? || DateTime.current.to_i - feed.last.created_time.to_i >= 4 * 24 * 60 * 60
        return to_return
      end
      max_id = feed.last.id
    end
  end

  def raw_media
    feed = client.user_recent_media(count: 100)
    feed.map! do |media|
      filter_media(media)
    end.compact
  end

  def all_media(user_id = nil, min_id = nil, limit = nil)
    media = []
    puts min_id
    response = client.user_recent_media(user_id, min_id: min_id)

    a = response.select { |media| media.type == 'image' }
    dup = a.select { |media| media.id == min_id }
    if dup.empty?
      media.concat(a)
    else
      a = a.slice(0, response.index(dup.first))
      media.concat(a)
      return media
    end

    counter = 0
    while response.pagination.next_max_id.present? && (limit.nil? || counter < limit)
      response = client.user_recent_media(user_id, max_id: response.pagination.next_max_id, min_id: min_id)
      a = response
      a.select! { |media| media.type == 'image' }
      dup = a.select { |media| media.id == min_id }
      if dup.empty?
        media.concat(a)
      else
        a = a.slice(0, a.index(dup.first))
        media.concat(a)
        break
      end
      counter += 1
    end

    media
  end

  def recent_media(user_id = nil)
    return client.user_recent_media(user_id).select { |media| media.type == 'image' }
  rescue
    []
  end

  def picture_info(picture_id)
    client.media_item(picture_id)
  rescue
    nil
  end

  def average(user_id)
    user = User.find_by_instagram_id(user_id.to_s) if user_id

    average = Average.find_by_user_id(user_id) if user_id
    return average.value if average && average.updated_at > 1.week.ago

    average = Average.new if average.nil?

    recent = recent_media(user_id)

    total = 0
    user = user_info(user_id)

    if user.nil?
      Average.delete(average.id) if average.id
      return [0, 0]
    end

    recent.each do |picture|
      total += picture.likes[:count]
    end

    average.user_id = user.id
    average.total = total
    average.followers = user.counts.followed_by
    average.pictures = recent.length
    average.save!

    average.value
  end

  def follow(user_id)
    client.follow_user(user_id)
  end

  private

  def client
    Instagram.client(access_token: @access_token)
  end

  def filter_media(media)
    if media.type == 'image'
      media.small_link = media.images.low_resolution.url
      media.large_link = media.images.standard_resolution.url
      media.delete(:images)

      media.posted_at = Time.at(media.created_time.to_i)
      media.delete(:created_time)
      media.poster = media.user
      media.delete(:user)
      media
    else
      nil
    end
  end

  def filter_and_strip_media(media, time = 18_000)
    if media.type == 'image' && DateTime.current.to_i - media.created_time.to_i < time
      %w(attribution tags type location comments filter users_in_photo caption
         user_has_liked link).each do |attr|
        media.delete(attr)
      end

      media.likes = media.likes[:count]
      media.small_link = media.images.low_resolution.url
      media.large_link = media.images.standard_resolution.url
      media.delete(:images)

      media.posted_at = Time.at(media.created_time.to_i)
      media.delete(:created_time)
      media.poster = media.user
      media.delete(:user)
      media
    else
      nil
    end
  end
end
