# User
class User < ActiveRecord::Base
  has_many :bets, ->{order('end_time DESC')}, class_name: 'Bet', inverse_of: :better, foreign_key: 'better_id', primary_key: 'instagram_id'

  class << self
    def user_from_instagram(user_info, access_token, dummy = false, temp = false)
      user = User.new(
          username: user_info.username,
          bio: user_info.bio,
          website: user_info.website,
          profile_picture: user_info.profile_picture,
          full_name: user_info.full_name,
          follower_number: user_info.counts.followed_by,
          following_number: user_info.counts.follows,
          instagram_id: user_info.id,
          access_token: access_token,
          dummy: dummy
      )
      user.save! unless temp
      if user.follower_number > 100000
        user.delete
        user = nil
      end
      user
    end

    def temp_user(user_info, access_token)
      user = User.new(
        username: user_info.username,
        bio: user_info.bio,
        website: user_info.website,
        profile_picture: user_info.profile_picture,
        full_name: user_info.full_name,
        follower_number: user_info.counts.followed_by,
        following_number: user_info.counts.follows,
        instagram_id: user_info.id,
        access_token: access_token,
        dummy: true
      )
    end
  end

  def feed(max_id = nil)
    feed = client.feed(max_id)
    feed.select do |picture|
      if BlockedUser.show_photo?(picture.poster.id.to_s, picture.id, self.id)
        picture.average > 100*picture.likes.to_f / picture.poster.followers && bets.where(picture_id: picture.id).first.nil?
      end
    end
  end

  def as_json(options = {})
    json = {
      username: username,
      bio: bio,
      website: website,
      profile_picture: profile_picture,
      full_name: full_name,
      followers: follower_number,
      following: following_number,
      id: instagram_id,
      money: money,
      # average: average.round(2),
      # picture_count: pictures.length,
      # best_pictures: pictures.sort_by { |pic| -pic.likes }[0...5]
    }

    json.merge!(auth_token: auth_token) if options[:show_auth_token]
    json.merge!(pictures: pictures.order(likes: :desc).slice(0, 9).map{ |pic| pic.as_json(no_poster: true)}) if options[:show_pictures]
    if options[:show_stats]
      json.merge!(average: client.average(instagram_id)[0].round(2))
      done = bets.select(&:done?).count
      if done == 0
        wins = 0
      else
        wins = bets.select(&:won?).count / done.to_f
      end
      json.merge!(wins: wins)
      json.merge!(bets: bets.select(&:done?).count)
      json.merge!(friends: friend_count)
      # json.merge!(rank: User.all.order('money DESC').index(self))
      json.merge!(rank: ranking)
    end
    if options[:show_average]
      json.merge!(average: client.average(instagram_id)[0].round(2))
    end
    json
  end

  def new_media
    recent_picture = pictures.order('posted_at DESC').first
    max_id = recent_picture.present? ? recent_picture.instagram_id : nil

    client.all_media(instagram_id, max_id)
  end

  def top_media
    client.all_media(instagram_id, nil, 5)
  end

  def pictures
    Picture.where(poster_id: instagram_id)
  end

  def average
    client.average(instagram_id)
  end

  def ranking
    follows = followings
    follows = follows.select do |follow|
      follow.following.present?
    end

    follows.count - follows.select do |follow|
      follow.following.money > money
    end.count + 1
  end

  def friend_count
    follows = followings
    follows.select do |follow|
      follow.following.present?
    end.count
  end

  def followers(_users = true)
    Follow.where(following_id: instagram_id)
  end

  def followings
    Follow.where(follower_id: instagram_id)
  end

  def all_followers
    client.followers
  end

  def all_following
    client.following
  end

  def best_followers
    media = client.raw_media
    users = Hashie::Mash.new
    media.each do |m|
      m.comments.data.each do |c|
        u = c.from
        if users.key?(u.id)
          users[u.id].comments = users[u.id].comments.to_i + 1
          #sum
          users[u.id].total = users[u.id].total.to_i + 1
        else
          users[u.id] = u
          users[u.id].comments = 1
          users[u.id].total = 1
        end
      end
      m.likes.data.each do |like|
        if users.key?(like.id)
          users[like.id].likes = users[like.id].likes.to_i + 1
          #sum
          users[like.id].total = users[like.id].total.to_i + 1
        else
          users[like.id] = like
          users[like.id].likes = 1
          users[like.id].total = 1
        end
      end
    end
    users.each do |id, u|
      if u.comments.nil?
        u.comments = 0
      end
      if u.likes.nil?
        u.likes = 0
      end
      if u.likes == 0 && u.comments == 0
        u.total = 0
      end
    end
    users.values.sort_by { |u| [-u.total] }
  end

  def update_relationships(async = true)
    if async
      FollowJob.new.async.perform(id)
    else
      FollowJob.new.perform(id)
    end
  end

  def update_media(async = true)
    if async
      UpdateMediaJob.new.async.perform(id)
    else
      UpdateMediaJob.new.perform(id)
    end
  end

  def update_self(user_info)
    if self.dummy?
      if self.updated_at < 1.day.ago
        User.delete(self.id)
      end
      return
    end
    self.username = user_info.username
    self.bio = user_info.bio
    self.website = user_info.website
    self.profile_picture = user_info.profile_picture
    self.full_name = user_info.full_name
    self.follower_number = user_info.counts.followed_by
    self.following_number = user_info.counts.follows
    save!
  end

  def client
    InstagramClient.new(access_token)
  end
end
