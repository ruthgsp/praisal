class BlockedUser < ActiveRecord::Base
  has_many :user_reports


  class << self
    def generate_block_user(poster_id)
      block_report = BlockedUser.find_by_instagram_id(poster_id)
      if block_report.nil?
        user = User.find_by_instagram_id(poster_id)
        block_report = BlockedUser.new(
            user_id: user.nil? ? nil : user.id,
            instagram_id: poster_id,
            block_count: 1
        )
      else
        block_report.block_count =  block_report.block_count + 1
      end
      block_report.save!
      block_report
    end

    def show_photo?(instagram_id, photo_id, user_id)
      block_record = BlockedUser.find_by_instagram_id(instagram_id)
      if block_record.nil?
        true
      else
        user_report = block_record.user_reports.where(blocked_by: user_id, photo_id: photo_id)
        show_this = block_record.block_count >= 10 || !user_report.empty?
        !show_this
      end
    end
  end


end
