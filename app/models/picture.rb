class Picture < ActiveRecord::Base
  has_many :bets, inverse_of: :picture, primary_key: 'instagram_id'

  class << self
    def picture_from_instagram(picture_info, temp = false)
      picture = Picture.new(
        large_link:   picture_info.images.standard_resolution.url,
        small_link:   picture_info.images.low_resolution.url,
        posted_at:    Time.at(picture_info.created_time.to_i),
        likes:        picture_info.likes[:count],
        poster_id:    picture_info.user.id,
        instagram_id: picture_info.id
      )
      picture.save! unless temp

      picture
    end
  end

  def as_json(options = {})
    json = {
      id: instagram_id,
      large_link: large_link,
      small_link: small_link,
      posted_at: posted_at,
      likes: likes
    }
    if options[:no_poster].nil?
      temp_poster = poster
      json[:poster] = temp_poster if temp_poster.present?
    end
    json
  end

  def update_self
    user = poster
    if user.nil?
      self.large_link = nil
      self.small_link = nil
      self.save!
      return
    end
    pic_info = user.client.picture_info(instagram_id)
    if pic_info.nil?
      puts "Picture #{instagram_id} has been deleted."
      self.large_link = nil
      self.small_link = nil
      self.save!
      bets.each do |bet|
        if not bet.done?
          bet.delete_self
        end
      end
      if bets.length == 0
        Picture.delete(id)
      end
      return
    end
    self.likes = pic_info.likes[:count]
    bets.each do |bet|
      puts "bet for this picture is #{bet.final_likes}"
      bet.final_likes = self.likes
      bet.save!
      puts "likes after is #{bet.final_likes}"
    end
    self.save!
  end

  def poster
    user = User.find_by_instagram_id(poster_id)
    if user.nil? || user.dummy?
      bet = Bet.find_by_picture_id(instagram_id)

      if bet.present?
        better = bet.better
        user_info = better.client.user_info(poster_id)
        return nil if user_info.nil?
        user = User.temp_user(user_info, better.access_token)
      else
        return nil
      end
    end

    user
  end
end
