class FollowLog < ActiveRecord::Base
  def follower
    user = User.find_by_instagram_id(follower_id)
    if user.nil?
      owner = User.find_by_instagram_id(user_id)
      user_info = owner.client.user_info(follower_id)
      if user_info.present?
        user = User.temp_user(user_info, owner.access_token)
        user.save!
      else
        user = nil
      end
    end
    user
  end
end
