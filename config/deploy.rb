require 'net/ssh/proxy/command'

# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'Praisal'
set :repo_url, 'ssh://git@bitbucket.org/ruthgsp/praisal.git'

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'config/instagram.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'config/thin')

set :keep_releases, 5

namespace :deploy do
  after :publishing, :restart
end