server 'ec2-52-8-162-232.us-west-1.compute.amazonaws.com',
       user: 'deploy',
    roles: %w{app db web},
    ssh_options: {
    proxy: Net::SSH::Proxy::Command.new('ssh -W %h:%p praisal@ec2-52-8-162-232.us-west-1.compute.amazonaws.com')

}

set :stage, :production
set :branch,    'master'
set :deploy_to, '/home/deploy/production'
set :rails_env, 'production'
