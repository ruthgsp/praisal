Rails.application.routes.draw do
  scope '/oauth', controller: 'oauth' do
    get '/', action: 'authorize'
    get '/callback', action: 'login'
    get '/logout', action: 'logout'
    get 'debug_login', action: 'debug_login'
  end

  root 'index#index'
  get '/eula', to: 'index#eula'
  get '/favicon.ico', controller: :index, action: :index
  resources :users, only: [:index, :show] do
    collection do
      get '/feed', action: :feed
      get '/friends', action: :friends
      get '/popular_ranking', action: :popular_friends
      get '/score_ranking', action: :rich_friends
      #get '/all_friends', action: :all_friends
      post '/friends', action: :make_friend
      get '/trending_photos', action: :trending_photos
      get '/new_followers', action: :new_followers
      get '/not_followers', action: :not_followers
      get '/best_followers', action: :best_followers
      get '/not_following', action: :not_following
      post '/device_token', action: :store_device_token
      post '/clear_notifications', action: :clear_notifications
      post '/invite', action: :invite_friend
      post '/redeem_invite', action: :redeem_invite
      post '/report_user', action: :flag_photo
    end
  end

  scope '/debug', controller: 'debug' do
    get '/set_points', action: 'set_points'
    get '/start', action: 'start_updater'
    get '/stop', action: 'stop_updater'
    get '/auth_token', action: 'get_auth_token'
    get '/push', action: 'push'
    get '/device_token', action: 'device_token'
    get '/access_token', action: 'access_token'
    get '/clear_bets', action: 'clear_bets'
    get '/update_pictures', action: 'update_pictures'
    get '/validate_follows', action: 'check_follows'
  end

  resources :bets, only: [:index, :create]
end
