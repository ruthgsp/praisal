require 'instagram'

INSTAGRAM_CONFIG  = YAML.load_file("#{Rails.root}/config/instagram.yml")[Rails.env].symbolize_keys

Instagram.configure do |config|
  config.client_id = INSTAGRAM_CONFIG[:app_id]
  config.client_secret = INSTAGRAM_CONFIG[:app_secret]
end
