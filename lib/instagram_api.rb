require 'instagram'

module InstagramApi

  def authorization_url
    Instagram.authorize_url(redirect_uri: url, scope: "relationships")
  end

  def exchange_code(code)
    begin
      Instagram.get_access_token(code, redirect_uri: url)
    rescue; end
  end

  def url
    INSTAGRAM_CONFIG[:app_callback]
    #if ENV['RAILS_ENV'] == 'production'
    #  'http://praisal.herokuapp.com/oauth/callback'
    #elsif ENV['RAILS_ENV'] == 'staging'
    #  'http://praisal.sourcepadstage.com/'
    #else
    #  #'http://localhost:3000/oauth/callback'
    #  'http://192.168.1.100:3000/oauth/callback'
    #end
  end
end