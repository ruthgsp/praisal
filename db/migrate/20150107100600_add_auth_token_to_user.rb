class AddAuthTokenToUser < ActiveRecord::Migration
  def change
    add_column :users, :auth_token, :string
    add_column :users, :login_count, :integer, default: 0
  end
end
