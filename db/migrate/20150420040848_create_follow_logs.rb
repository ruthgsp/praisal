class CreateFollowLogs < ActiveRecord::Migration
  def change
    create_table :follow_logs do |t|
      t.string      :user_id
      t.boolean     :added
      t.string      :follower_id
      t.timestamps
    end

    add_index :follow_logs, :user_id
  end
end
