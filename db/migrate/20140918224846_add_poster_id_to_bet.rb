class AddPosterIdToBet < ActiveRecord::Migration
  def change
    add_column :bets, :poster_id, :string
  end
end
