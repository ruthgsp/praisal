class AddIndexes < ActiveRecord::Migration
  def change
    add_index :follows, :follower_id
    add_index :follows, :following_id
    add_index :bets, :better_id
    add_index :pictures, :poster_id
    add_index :pictures, :instagram_id
    add_index :users, :auth_token
  end
end
