class ChangeFollowsTable < ActiveRecord::Migration
  def self.up
    change_column :follows, :follower_id, :string
    change_column :follows, :following_id, :string
  end
 
  def self.down
    change_column :follows, :follower_id, :integer
    change_column :follows, :following_id, :integer	
  end
end
