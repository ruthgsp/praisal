class CreateBets < ActiveRecord::Migration
  def change
    create_table :bets do |t|
      t.string   :better_id
      t.string   :picture_id
      t.integer  :betted_likes
      t.integer  :betted_money
      t.datetime :end_time
      t.integer  :final_likes

      t.timestamps
    end
  end
end
