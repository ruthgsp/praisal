class ChangeAllIntegerToDecimal < ActiveRecord::Migration
  def self.up
    change_column :averages, :total, :decimal    
    change_column :averages, :followers, :decimal
    change_column :averages, :pictures, :decimal    
    change_column :averages, :user_id, :string

    change_column :bets, :betted_likes, :decimal
    change_column :bets, :betted_money, :decimal
    change_column :bets, :final_likes, :decimal

    change_column :pictures, :likes, :decimal

    change_column :users, :follower_number, :decimal
    change_column :users, :following_number, :decimal
    change_column :users, :money, :decimal
  end
 
  def self.down
  	change_column :averages, :total, :integer    
    change_column :averages, :followers, :integer
    change_column :averages, :pictures, :integer
    # change_column :averages, :user_id, :integer

    change_column :bets, :betted_likes, :integer
    change_column :bets, :betted_money, :integer
    change_column :bets, :final_likes, :integer

    change_column :pictures, :likes, :integer

    change_column :users, :follower_number, :integer
    change_column :users, :following_number, :integer
    change_column :users, :money, :integer
  end
end
