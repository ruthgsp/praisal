class AddColumnCollectedToBets < ActiveRecord::Migration
  def change
    add_column :bets, :collected, :boolean  , default: false
  end
end
