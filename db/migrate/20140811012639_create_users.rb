class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string  :username
      t.text    :bio
      t.text    :website
      t.text    :profile_picture
      t.string  :full_name
      t.integer :follower_number
      t.integer :following_number
      t.string  :access_token
      t.integer :money, default: 1000
      t.string  :instagram_id

      t.timestamps
    end

    add_index :users, :instagram_id
  end
end
