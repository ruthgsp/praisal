class CreateAverages < ActiveRecord::Migration
  def change
    create_table :averages do |t|
      t.integer     :user_id
      t.integer     :total
      t.integer     :followers
      t.integer     :pictures
      t.timestamps
    end

    add_index :averages, :user_id
  end
end
