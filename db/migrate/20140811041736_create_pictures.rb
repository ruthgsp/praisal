class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.text     :large_link
      t.text     :small_link
      t.datetime :posted_at
      t.integer  :likes
      t.string   :poster_id
      t.string   :instagram_id

      t.timestamps
    end
  end
end
