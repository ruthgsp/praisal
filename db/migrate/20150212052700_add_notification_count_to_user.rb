class AddNotificationCountToUser < ActiveRecord::Migration
  def change
    add_column :users, :notification_count, :integer
  end
end
