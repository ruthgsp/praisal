class CreateUserReports < ActiveRecord::Migration
  def change
    create_table :user_reports do |t|
      t.integer :blocked_user_id
      t.integer :blocked_by
      t.integer :block_this
      t.string :blocked_instagram_id
      t.string :photo_id
      t.string :reason

      t.timestamps
    end
  end
end
