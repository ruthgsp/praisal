class AddReverseToFollow < ActiveRecord::Migration
  def change
    add_column :follows, :reverse, :boolean, default: false
  end
end
