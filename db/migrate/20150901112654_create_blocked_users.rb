class CreateBlockedUsers < ActiveRecord::Migration
  def change
    create_table :blocked_users do |t|
      t.integer :user_id
      t.string :instagram_id
      t.integer :block_count

      t.timestamps
    end
  end
end
