# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150927034347) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "averages", force: true do |t|
    t.string   "user_id"
    t.decimal  "total"
    t.decimal  "followers"
    t.decimal  "pictures"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "averages", ["user_id"], name: "index_averages_on_user_id", using: :btree

  create_table "bets", force: true do |t|
    t.string   "better_id"
    t.string   "picture_id"
    t.decimal  "betted_likes"
    t.decimal  "betted_money"
    t.datetime "end_time"
    t.decimal  "final_likes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "poster_id"
    t.boolean  "collected",    default: false
  end

  add_index "bets", ["better_id"], name: "index_bets_on_better_id", using: :btree

  create_table "blocked_users", force: true do |t|
    t.integer  "user_id"
    t.string   "instagram_id"
    t.integer  "block_count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "follow_logs", force: true do |t|
    t.string   "user_id"
    t.boolean  "added"
    t.string   "follower_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follow_logs", ["user_id"], name: "index_follow_logs_on_user_id", using: :btree

  create_table "follows", force: true do |t|
    t.string   "follower_id"
    t.string   "following_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "reverse",      default: false
  end

  add_index "follows", ["follower_id"], name: "index_follows_on_follower_id", using: :btree
  add_index "follows", ["following_id"], name: "index_follows_on_following_id", using: :btree

  create_table "pictures", force: true do |t|
    t.text     "large_link"
    t.text     "small_link"
    t.datetime "posted_at"
    t.decimal  "likes"
    t.string   "poster_id"
    t.string   "instagram_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pictures", ["instagram_id"], name: "index_pictures_on_instagram_id", using: :btree
  add_index "pictures", ["poster_id"], name: "index_pictures_on_poster_id", using: :btree

  create_table "user_reports", force: true do |t|
    t.integer  "blocked_user_id"
    t.integer  "blocked_by"
    t.integer  "block_this"
    t.string   "blocked_instagram_id"
    t.string   "photo_id"
    t.string   "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.text     "bio"
    t.text     "website"
    t.text     "profile_picture"
    t.string   "full_name"
    t.decimal  "follower_number"
    t.decimal  "following_number"
    t.string   "access_token"
    t.decimal  "money",              default: 1000.0
    t.string   "instagram_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "dummy",              default: false
    t.string   "auth_token"
    t.integer  "login_count",        default: 0
    t.text     "device_token"
    t.integer  "notification_count"
    t.boolean  "is_new",             default: true
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", using: :btree
  add_index "users", ["instagram_id"], name: "index_users_on_instagram_id", using: :btree

end
